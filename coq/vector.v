Require Vector.
Import Vector.VectorNotations.
Print Vector.t.



Print list.
Check cons.

Fixpoint len {A} (l : list A) : nat :=
  match l with
  | nil => 0
  | cons _ lp => 1 + len lp
  end.

Check len.

Fixpoint lengthHardwork {n}{A}(v : Vector.t A n) : nat :=
  match v with
  | [] => 0
  | (_ :: xs) => 1 + lengthHardwork xs
  end.

Definition length {n} {A} : Vector.t A n ->  nat := fun _ => n.

(*


  A : Type
  ============================
  0 = length []

subgoal 2 (ID 39) is:
 S (lengthHardwork v) = length (h :: v)


 *)

Fixpoint iterate {A} n (f : A -> A) (a0 : A) :=
  match n with
  | 0 => a0
  | S m => f (iterate m f a0)
  end.

Compute iterate 10 S 1.

Lemma iterate_lemma : forall n, iterate n S 1 = S n.
  intros.
  induction n; trivial.
  simpl.
  rewrite IHn; trivial.
Qed.

Definition myS n := iterate n S 1.


Definition tail1 {A n} (v : Vector.t A (S n)) : A :=
  match v with
  | x :: _ => x
  end.

Print tail1.

(*
Coq does not know S n is myS n

 *)

(* This will not compile
Definition tail {A n} (v : Vector.t A (myS n) ) : A :=


  match v with
  | x :: _ => x
  end.

 *)

Check {True} + {False}.
Print sumbool.

(* sumbool is a proof relevant version of boolean.

A \/ B

*)


Definition tail {A n} (v : Vector.t A (S n)) : Vector.t A n :=
  match v with
  | x :: xs => xs
  end.

Print tail.


Definition pred (n : nat) (pf : n > 0) : nat.
  refine( match n with
          | S m => m
          | 0   => 0
          end).
Defined.

Require Extraction.
Extraction pred.

Print sumbool.
Print IDProp.
Check idProp.


Inductive FProp : Prop := F | FF | FFF.
Inductive NProp : Prop :=
| Zero : NProp
| Succ : NProp -> NProp.

Definition bar (x : FProp) : NProp :=
  match x with
  | F => Zero
  | FF => Succ Zero
  | FFF => Succ (Succ Zero)
  end.

Extraction bar.

Definition fooP (x : FProp) : nat := 2.
Check fooP.

(*

This code does not work
Definition foo (x : FProp) : nat :=
  match x with
  | F => 2  (* It might be an aribitrary complicated exp that gives 2 *)
  | FF => 2
  | FFF => 2
  end.
*)

(*
foo x is not "proof irrelevant"
*)
Section Take.
  (*
  Variable A : Type. *)

  Context {A : Type}.
  Definition head {n} (v : Vector.t A (S n) ) : A :=
    match v with
    | x :: _ => x
    end.

  Definition tl {n} (v : Vector.t A (S n) ) : Vector.t A n :=
    match v with
    | _ :: xs => xs
    end.

  Check head.



  Fixpoint take {n}  m (pf : m <= n) (v : Vector.t A n) : Vector.t A m.
    refine (
        match m, v with
        | 0   , _      => []
        | S mp, x :: xs => x :: @take _ mp _ xs
        | S mpp, [] => _
        end).
    (* There is no way to continue with this *)
  Abort.

  Fixpoint take {n} m (pf : m <= n) : Vector.t A n -> Vector.t A m.
    refine (
        match m, n with
        | 0 , _          => fun _ => []
        | S mp, S np     => fun v => head v :: @take np mp _ (tail v)  (* interesting case *)
        | S mpp, 0       => _ (* first goal *)
        end).
  Abort.

  Check (true, false, True, False).
  Print True.
  Print False.
  Lemma proof_of_truth : True.
    exact I.
  Qed.

  Definition absurdity {A: Type} (pf : False) : A
    := match pf with
       end.
  Check absurdity.


  Check false.


  Fixpoint take {n} (m : nat)
    : m <= n -> Vector.t A n -> Vector.t A m.
    refine(
        match m, n with
        | 0, _ => fun _ _ => []
        | S mp, S np => fun pf v =>
                         let f := head v in
                         let rest := @take np mp _ (* mp <= np *) (tail v) in
                         f :: rest
        | S mpp, 0  => fun pfSmppLe0 v =>
                         let ab : False := _ in
                         absurdity ab
        end).
    inversion pfSmppLe0.

    apply le_S_n; trivial.
    Show Proof.
    Search (0 <= _).

  Defined.


  (* Qed.*)
  (* Defined. *)

  Fixpoint qtake {n} m : m <= n -> Vector.t A n -> Vector.t A m.
    refine(
        match m, n with
        | 0, _ => fun _ _ => []
        | S mp, S np => fun pf v => head v :: @take np mp _ (tail v)
        | S mpp, 0  => _
        end).
    intro hyp.
    assert (Ab:False) by  inversion hyp.
    destruct Ab.
    apply le_S_n; trivial.
    Show Proof.
    Search (0 <= _).

    Qed.

End Take.

Lemma oneLE3 : 1 <= 3.
  repeat constructor.
Qed.

Require Extraction.

Extraction take.

Compute oneLE3.

Compute (take 1 oneLE3 [1;2;3], qtake 1 oneLE3 [1;2;3]).






  Lemma succ_is_not_le_zero : forall n, S n <= 0 -> False.
  Proof.
    intros.
    inversion H.
  Qed.

  Print nat.
  Print eq.

  Inductive myeq (B : Type) (x : B) : B -> Prop :=
    myeq_refl : myeq B x x.

  Inductive prod (A B : Type) : Type :=
    mkProd : A -> B -> prod A B.

  Locate "*".
  Check (nat * bool)%type.

  Check nat -> bool.

  Print and.
  Print or.
  Print ex. (* is like  *  but (x , p) where x : A , p : P x ) *)
      Search ( forall _ : Type, (_ -> Prop) -> Prop).
      (*

 A : Type




 B : A -> Type   B is the constant function a -> C

 (forall a : A, B a)  is a type. Pi-type

forall a : A, B a   forall a : A, C  = A -> B


*)

  (*

    A -> B : is the type of (ordinary) functions from A to B

A as the type of proof objects that convince me of A

Corresponds to the implication [A] => [B]






  forall x : A, B x : Type of dependent functions

It takes  a : A to (B a)

How can you convince me of the claim forall x : A, B x

By giving a dependent function  to map a : A to pf : B a


   *)

  Lemma foo : forall n : nat, 0 + n = n.
    intro.
    Show Proof.
    Show Proof.


    (* A , x y : A | x = y : Type

        nat : Type, n : nat, 0 : nat  |- 0 + n : nat
        nat : Type, n : nat, 0 : nat  |- 0 + n = n  : Type
        B : nat -> Type
        forall n : nat , B n is a Type
       B (n) := 0 + n = n

    *)

    trivial.
  Qed.

  Lemma foo1 : forall n : nat, myeq nat (0 + n) n.
    intros.
    simpl.
    constructor.
  Qed.
  (*
  Lemma bar : forall n : nat, n + 0 = n.
    trivial.
   Qed.

   *)


Check (head, @head nat 10, head (A:=nat)).
Check head (n:=1) (A:=nat).



  (*
Fixpoint spooky_take {A n}(v : Vector.t A n) m (cond : {m <= n} + {m > n}) :
  match cond with
  | left _ => Vector.t A m
  | _      => IDProp
  end.
  refine ( match cond with
           | right _   => idProp
           | left mLEn =>

             m , v with
         | 0    , _         => []
         | S mp , x :: xs    => x :: spooky_take xs mp _
         | _ , _        => _
         end).

    )


*)
(* This is what we want *)

Definition tl1 {A n}(v : Vector.t A (S n)) : A :=
  match v
        as v0 (* another name of the matched expression  *)

        in (* the type of the expression being matched *)
        Vector.t _ n0
                 (* n0 is bound to that n such that v : Vector.t _ n *)

        return (* return type of the match *)
             match n0 with
             | 0 => IDProp
             | S _ => A
             end
   with
   | []  (* I am matching v with []
            v0 is bound to []
            n0 is bound to 0
          *)


     => (* My goal here is to fill in a value of type

            IDProp
        *)

     idProp

   | x :: _

       (* v is in Vector.t _ (S m0)
               in Vector.t _ n0
         n0 is bound to (S m0)

       *)

     =>  (* I have to decide what type of value I have to fill here

             match S m0 with
             | 0 => IDProp
             | S _ => A
             end


         *)

x
   end.

Definition complicated_tail {A n} (v : Vector.t A (S n)) : A :=
  match v in Vector.t _ n0
        return match n0 with
               | 0 => bool
               | _ => A
               end
  with
  | []  => true
  | x :: _ => x
  end.



Print tail.


Definition both_are_same : forall n A (v : Vector.t A n), lengthHardwork v = length v :=

(fun (n : nat) (A : Type) (v : Vector.t A n) =>
 Vector.t_ind A (fun (n0 : nat) (v0 : Vector.t A n0) => lengthHardwork v0 = length v0) eq_refl
   (fun (h : A) (n0 : nat) (v0 : Vector.t A n0) (IHv : lengthHardwork v0 = length v0) =>
    eq_ind_r (fun n1 : nat => S n1 = length (h :: v0)) eq_refl IHv) n v).


Ltac silly_ring := idtac "Hello world".

Require Import Arith.
Lemma square_formula : forall a b : nat, (a + b)^3 = a^3 + b^3 + 3 * a^2 * b + 3 * a * b^2.
Proof.
  intros.
  simpl.
  ring.
  Show Proof.
Qed.



Lemma zngtz : (0 < 0) -> False.
  intro.
  inversion H.

  (*

    variables
    functions

    nat

    T -> T'  is just the type of  functions from T to T'


    1.  f : A -> B
    2.  x : A
    ==================
       f x : B                (f function applied on the value x)


     x : A  ⊢ e : B
    ===================
     ̧⊢  fun x => e : A -> B


     x : A   as in a programming language  x is a value of type A
             as a logic you can see it as x is the proof of the statement A
  *)
Qed.


Lemma false_implies_anything : forall A : Type, False -> A.
  intros.
  destruct H.
Qed.

Program Definition pred1 n (pf : n > 0) : nat :=
  match n with
  | 0 => _
  | S m => m
  end.
Next Obligation.
  apply false_implies_anything.
  apply zngtz. trivial.
  Show Proof.
Defined.

(* Functions can be dependently typed.

f : forall x : A, B(x)

f is a function that takes x : A  to  B(x)

Type A are Mathematical statements (that one tries to prove)

and x : A is a proof of A

I want to talk about a predicate on nat

P(0) , P(1) .... P(n) ....

Each can be either true or false

P(0) is a type  x₀ : P(0) is a proof of P(0)
P(1) is a type  x₁ : P(1)
...
..

A predicate on a type A is nothing but a type family P : A -> Type
And proving forall a : A, P(a) is nothing but giving a function f
such that f a  : P (a)


So how can I convince you of forall n : nat, P(n)

By given a dependently typed function f



 *)



Definition predP (n : nat) :
  match n with
  | 0 => unit
  | _ => nat
  end :=

  match n with
  | 0 => tt
  | S m => m
  end.

Program Definition pred' (n : nat) (pf : n > 0) : nat :=
  match n with
  | 0 => 0
  | _ => _
  end.

Compute predP 10.


Compute pred 0 _.



(*
Definition pred n (pf : n > 0) : nat.
  refine (match n with
          | 0 => _
          | S m => m
          end).
Show Proof.
 *)





(*

1. Vernacular        - Things that control your interaction with Coq
2. Gallina           - Functional programming language
3. Ltac or tactics   - Tactic language is untrusted (like heuristics)

The only thing that is trusted is the Gallina's type checking.

Lemma foo : Statement.
Proof
        tactics here
End

Statement is a type and foo is an element of that type

foo : A

Lemma foo : Something := .....
 *)
